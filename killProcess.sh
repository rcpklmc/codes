########################
#@author Recep Kalmaç
########################

#!/usr/bin/env bash

while true

do

echo -e "enter the process name to kill : \c"

read process_name


processingId=`ps aux | grep -v grep | grep $process_name | awk '{print $3}'`

#echo "process id : $processingId"

if [ "$process_name" == "" ]; then

echo "Enter a process name!!!";

fi

if [ ! $process_name == "" ]

    then

    if [ -z $processingId ]

    then

    echo "no process is running like $process_name......"

    break

    fi

     ps -ef | grep $process_name |grep -v grep | awk '{print $2}' | xargs kill

fi

echo -e "Do you want to go on? [Y-N]: \c"

read answer

case $answer in

[nN] )

    exit ;;

[yY] )

echo "go on to kill process"

echo;;

*)

echo "invalid input!!!!"

exit

esac

done