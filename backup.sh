#!/bin/bash

#@author Recep Kalmaç
#open-source community
## you should enter the name of the folder when running the script. Example : sh backup.sh MYFOLDER

cd ~/Desktop/
mkdir storage

#checking a folder name is entered or not

if [ $# != 1 ]

then
clear
echo "Enter the folder which is storaged"
exit

fi

if [ ! -d  /storage/$1 ];then
clear
echo "There is no folder you entered.New folder is created"
mkdir /tmp/$1&&touch /tmp/$1/file.txt /tmp/$1/file2.txt
echo
echo $1 folder and it\'s contains is created at /tmp folder

sleep 4

fi

echo "///////////////////////////////////"

date=`date + %F`

###checking the another folder is backed up before

if [ -d /tmp/backups/$1_$date ]; then

echo "the folder is backed up.Do you want to rewrite over it?"
read user_answer

    if [$user_answer !=  'y' | $user_answer != 'Y']
    then
    exit
    fi
else

mkdir -p /tmp/backups/$1_$date
echo /backup folder and $1_$date folder is created at /tmp folder
sleep 4

fi

cp -R /tmp/$1/tmp/backups/$1_$date
echo $1 folder\'s container at /tmp folder is backed up at /tmp/backups/$1_$date
sleep 4

echo
echo "back up operation is finished successfully :--)))"

echo /tmp folder is removed when the system is power off
sleep 2

echo "main folder container "

ls -l /tmp/$1
sleep 1

echo "back up folder container"
ls -l /tmp/backups/$1_$date/$1/

echo "the end of the process "
echo "exiting ..........."
sleep 3