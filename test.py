print("-----------------------------------------------------")
print("A simple __init__ and __def__ class constructors	 ")

class Person:
	
	def __init__(self):
	    print ("our class is created")	
	def setName(self,firstname):
	    self.firstname = firstname
	def printName(self):
            print("name : " , self.firstname)
	def __del__(self):
	    print("our class is destroyed")


info=Person()
info.setName("KaliLinux")
info.printName()

print("--------------------------------------------------------")
print("Subclasses , Superclasses , and Inheritance")

class Parent:

	value1 = "value1"
	value2 = "value2"

class Child (Parent) :
	pass

parent_= Parent()
print(parent_.value1)
print(parent_.value2)
child_ =  Child()
child2_ = Parent()
print("child : " ,child_.value1)
print("child2 : " , child2_.value1)
