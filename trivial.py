__author__ = "Recep Kalmaç"
#some kind of python code for beginners

import re

a = (1, 2, 3, "hello")  #   tuple
b = [1, 2, 3, "hello"]  #   list
print(type(a))
print(type(b))
print("-------------------------------------------------")

class Human:
    def __init__(self, kind="good"):
        self.kind = kind

    def whatKind(self):
        return self.kind

def main():
    GoodHuman = Human()
    print(GoodHuman.whatKind())
    BadHuman = Human("Bad")
    print(BadHuman.whatKind())

def count():
    strings = "this is a string"
    for index, s in enumerate(strings):
        if s == 's':
            print("'s' at located at position {} ".format(index))

def func():
    Test()

def Test():
    print("Test() function")
    Another()

def Another():
    print("Another() function")

def name():
    nameArguments(name='linux', distro='lubuntu')

def nameArguments(**a):
    for key in a:
        print(key, "=", a[key])

print("------------------------------------CLASS EXAMPLE------------------------------------------")

class Robots:

    def __init__(self):
        pass

    def WalkLikeARobot(self, style):
        self.style = style
        print(self.style)

    def CareLikeARobot(self):
        print("take care of your robot")

class Humans:
    def __init__(self, nature="good"):
        self.nature = nature

    def goodHuman(self):
        print("good human ----->", self.nature)

    def badHuman(self):
        self.nature = "bad"
        print("bad human -----------> ", self.nature)

    def WalkLikeARobot(self,style):
        self.style = style
        return self.style

class Height:
    def __init__(self):
        pass
    def set_height(self,height):
        self.height = height
    def  get_height(self):
        return self.height

print("---------------------------------POLYMORPHISM-----------------------")

class University:

    def __init__(self):
        pass
    def Student(self):
        print("Student of the university")
    def Class(self):
        print("Class of the university")

class School:

    def __init__(self):
        pass
    def Student(self):
        print("student of the school")
    def Class(self):
        print("class of the school")

print("-----------------------------INHERIANCE-----------------------------------")

class AllUser:
    def __init__(self):
        pass
    def __register__(self):
        print("all user register")

    def __login__(self):
        print("all user login")

class Admin(AllUser):

    def __init__(self):
        pass

    def __register__(self):
        print("admin register")

    def __login__(self):
        print("admin login")

class Members(AllUser):

    def __init__(self):
        pass

print("-------------------DECORATOR-------------------")

class Cat:

    def __init__(self,**rmk):
        self.properties = rmk
    @property

    def Name(self):
        return self.properties.get('name' , None)
    @Name.setter

    def Name(self,name):
        self.properties['name'] = name

    @Name.deleter
    def Name(self):
        del self.properties['name']

if __name__ == "__main__":

    main()
    count()
    func()
    name()
    print("-----------------------------")
    robot = Robots()
    robot.WalkLikeARobot("linux")
    robot.CareLikeARobot()
    human = Humans()
    human.goodHuman()
    human.badHuman()
    bad = Humans()
    bad.nature = "confused"
    bad.goodHuman()
    bad.badHuman()
    print(bad.WalkLikeARobot("hello again"))
    print("********************")
    height = Height()
    height.set_height(1.73)
    print(height.get_height())

    uni = University()
    sch = School()

    def Poly(University):
        University.Class()
        University.Student()
    def Morph(School):
        School.Class()
        School.Student()

    Poly(sch)
    Morph(uni)

    alluser = AllUser()
    alluser.__login__()
    alluser.__register__()
    member = Members()
    member.__register__()
    member.__login__()
    admin = Admin()
    admin.__register__()
    admin.__login__()

    brave = Cat()
    brave.Name = 'Cesur'
    print(brave.Name)

    print("--------------STRING METHODS ----------------------")
    trivial = "heLLo WorLd tHIs is A siMPLe TexT"
    print(trivial.swapcase())
    print(trivial.find('he'))
    print(trivial.lower())
    print(trivial.upper())
    print(id(trivial))
    text = trivial.replace('TexT','text')
    print(text)
    print(text.swapcase())
    print(id(text))

    print("------------------DICT---------------")
    dictionary = {'hello':'merhaba','good':'iyi','how':'nasıl'}
    for i in dictionary:print(i)
    for a,b in dictionary.items():print(a,":",b)





