#@author Recep Kalmaç
#!/usr/bin/env bash

echo -e "Create a file:\c"

read file_name

cd ~/Desktop/

touch $file_name

file_capacity=`ls -l "$file_name" | awk '{print $5}'` # byte format

echo "file capacity : $file_capacity byte(s)"

if [ -f $file_name ]

then

    until [ $file_capacity -ge "200" ]

    do

    echo "Test Message" >> "$file_name"

    file_capacity=`ls -l "$file_name" | awk '{print $5}'`

    echo "file capacity : $file_capacity byte(s)"

    done

    echo "file capacity at least 200 bytes "

    echo "Do you want to continue? [Y-N]"

    read answer

    case $answer in

    [yY] )

    echo "Hello World, GNU/Linux Debian is the best one :))" >> 	"$file_name"

    file_capacity=`ls -l "$file_name" | awk '{print $5}'`

    echo "file capacity : $file_capacity byte(s)"


    exit ;;

    [nN] )

    rm $file_name ;;

    *)

    esac

fi
