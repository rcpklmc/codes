"""
author : Recep Kalmaç
Qr Code Generator with Python 3 and Google API

"""

def qr_code_logo():
    qr_logo = """
▄▄▄▄▄▄▄ ▄▄  ▄ ▄▄▄▄▄▄▄ 
█ ▄▄▄ █ ▄▀▄ █ █ ▄▄▄ █ QR Code Terminal Generator
█ ███ █ █▄▄█  █ ███ █ 
█▄▄▄▄▄█ ▄ ▄ ▄ █▄▄▄▄▄█ Author : Recep Kalmaç
▄▄▄▄  ▄ ▄▀███▄  ▄▄▄ ▄ 
▀▄█▄ ▄▄███▀  ▄▄█▀ ███ 
▀▄▀▄  ▄▀▀ ▄█▄▄▄ ▀▀  ▄ 
▄▄▄▄▄▄▄ ▀  █▀ ███▄▀   
█ ▄▄▄ █  ▀▄ █▀ █▄██▄▀ 
█ ███ █ █▄█▄█    ▄▄▀  
█▄▄▄▄▄█ █▀▀▄ ██▀  ▄ ▀          
	       
	       """
    print(qr_logo)

def running_qr_code_generator():
    while True:
        qr_code_logo()
        qr_code_size = input("[?] Enter a size for QR Code      :  (Like : 100x100)")
        qr_data = input("[?] Enter a specific data for QR Code  :  (Like  : \nNews\nhttps://www.facebook.com\nRecepKalmac  )")
        google_API_url_path = "https://chart.googleapis.com/chart?"
        size = "chs="+qr_code_size
        image1 = "&"
        qr = "cht=qr"
        image2 = "&"
        data = "chl="+qr_data
        image3 = "&"
        encoding = "choe=UTF-8"
        generating = google_API_url_path+size+image1+qr+image2+data+image3+encoding
        print("[+]",generating)

if __name__ == '__main__':
    while True:
        print("************************************\nWelcome QR Code Generator\n**************************************")
        running_qr_code_generator()