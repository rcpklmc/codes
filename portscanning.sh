#!/bin/bash
#to run this code ,you should integrated this code to the .bashrc 
#then open new terminal and run the script
#@author recep kalmaç 

portscanning(){

for (( i = 0; i < 1; i++ )); do

	cd Desktop
	
	touch error.txt

	if [ -e error.txt ]
		
	then

		echo "error.txt is created in /home/$USER/Desktop to save your log"

	else

		echo "an error is occured when creating error.txt file"

fi

	#statements
done

if [ $# -eq 1 ]; then

for port in {1..1000}; do 

nc -z -v -w1 $1 $port >> /home/$USER/Desktop/error.txt 2>&1 && echo $port OPEN

# -z user for scanning , -v used for verbose output , -w1 timeout for connection is 1 second and final net reads

done

elif [ $# -eq 2 ]; then

  if (nc -z -v -w1 $1 $2 >> /home/$USER/Desktop/error.txt 2>&1); then

    echo $2 OPEN

  else
    
    echo $2 CLOSE

  fi

fi

}