#@author Recep Kalmaç
# simple code for writing file with Cesar Cipher
#Python 3.5.3

from time import sleep

alphabet_lower = "abcdefghijklmnopqrstuvwxyz"
alphabet_upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
alphabet_numbers = "0123456789"
alphabet_others = ".+-*/!'£>^#+$%½&¾/{([)]=}?_|<>|"
increase_const = 3
ceaser_encrypt = ""
ceaser_decrypt = ""

print("WRITING AND READING FILE IN PYTHON PROGRAMMING LANGUAGE")
print("........................................................")
sleep(3)
data_log = open("/root/Desktop/data.log","a")

# writing message to the file

user_input = input("Enter your message to the log file:")
if (data_log.writable()):
    for i in user_input:
        if i in alphabet_lower:
            index = alphabet_lower.index(i)
            newIndex = index+increase_const
            shifted = alphabet_lower[newIndex]
            ceaser_encrypt+=shifted

        elif i in alphabet_others:

            index = alphabet_others.index(i)
            newIndex = index + increase_const
            shifted = alphabet_others[newIndex]
            ceaser_encrypt += shifted
        elif i in alphabet_numbers:

            index = alphabet_numbers.index(i)
            newIndex = index + increase_const
            shifted = alphabet_numbers[newIndex]
            ceaser_encrypt += shifted
        elif i in alphabet_upper:

            index = alphabet_upper.index(i)
            newIndex = index + increase_const
            shifted = alphabet_upper[newIndex]
            ceaser_encrypt += shifted

        else:
            print("Invalid input!!!!")
    data_log.write(ceaser_encrypt)
    data_log.write("\n")
else:
    print("An error is occured while writing datas to file")
data_log.close()
