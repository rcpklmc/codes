import random
import string


def random_password_generator():
	chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
	size = 8
	return "".join(random.choice(chars)  for x in range(size,20))

print("password  : " + random_password_generator())

